var boxOpened = "";
var opened = "";
var counter = 0;
var found = 0;
var cardCount = 40;

var source = "#boxcard";

vocabulary = [
  {"czech": "navštěvovat", "pronunciation": "zára", "arabic": "زارَ"},
  {"czech": "točit se", "pronunciation": "dára", "arabic": "دارَ"},
  {"czech": "můj dům", "pronunciation": "dárí", "arabic": "داري"},
  {"czech": "přijít/dorazit", "pronunciation": "uarada", "arabic": "وَرَدَ"},
  {"czech": "údolí", "pronunciation": "uádí", "arabic": "وادي"},
  {"czech": "jádro", "pronunciation": "lubun", "arabic": "لُبٌّ"},
  {"czech": "chutný", "pronunciation": "lathíthun", "arabic": "لَذيذٌ"},
  {"czech": "země, stát", "pronunciation": "baladun", "arabic": "بَلَدٌ"},
  {"czech": "kopec, malá hora", "pronunciation": "talun", "arabic": "تَلٌّ"},
  {"czech": "ruka", "pronunciation": "jadun", "arabic": "يَدٌ"}
];

function equal(a, b){
    for(var i = 0; i < vocabulary.length; i++){
        if (vocabulary[i].czech == a){
            if (vocabulary[i].arabic == b)
                return true;
        }
        if (vocabulary[i].czech == b){
            if (vocabulary[i].arabic == a)
                return true;
        }
    }
    return false;
}

function OpenCard() {
	var id = $(this).attr("id");

	if ($("#" + id + " span").is(":hidden")) {
		$(source + " div").unbind("click", OpenCard);
		$("#" + id + " span").fadeIn();
		if (opened == "") {
			boxOpened = id;
			opened = $("#" + id + " span.word").text();
			setTimeout(function() {
				  $(source + " div").bind("click", OpenCard);
			}, 300);
		} else {
			currentOpened = $("#" + id + " span.word").text();
			if (!equal(opened, currentOpened)) {
				setTimeout(function() {
					$("#" + id + " span").delay(1500).fadeOut();
					$("#" + boxOpened + " span").delay(1500).fadeOut();
					boxOpened = "";
					opened = "";
				}, 400);
			} else {
				$("#" + id).addClass('correct');
				$("#" + boxOpened).addClass('correct');
				$("#" + id).delay(3000).fadeTo(1500,0);
				$("#" + boxOpened).delay(3000).fadeTo(1500,0);
				found++;
				boxOpened = "";
				opened = "";
			}
			setTimeout(function() {
				  $(source + " div").bind("click", OpenCard);
			}, 1500);
		}
		counter++;
		$("#counter").html("" + counter);
		if (found >= vocabulary.length) {
			$("#counter").prepend('<span id="success">Výhra během </span>');
		}
	}
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
}

function reset(){
    boxOpened = "";
    opened = "";
    $(source+" div").remove();
    $('#success').remove();
    counter = 0;
    found = 0;
    $("#counter").html("" + counter);
    init();
}

function parseSheet(data){
    vocabularyAll = data.feed.entry.map(function(row){
        return {'czech': row['gsx$česky']['$t'],
                  'pronunciation': row['gsx$výslovnost']['$t'],
                  'arabic': row['gsx$arabsky']['$t'],
                  'lecture': row['gsx$lekce']['$t']};
        }).filter(function (entry){
            return (entry.czech.match(/\S/)) && (entry.arabic.match(/\S/)) &&
                   (entry.pronunciation.match(/\S/)) && (entry.lecture.match(/\S/));
        });
}


function init() {
    var vocArr = new Array();
    shuffle(vocabularyAll);
    vocabulary = vocabularyAll.filter(function(word){
        return word.arabic.slice(-1) == "َ" || !$('#verbs').prop('checked');
    }).slice(0,Math.floor(cardCount/2));
    $.each(vocabulary, function(i, val) {
		vocArr.push({"type": "cz", "word": val.czech, "lecture": val.lecture});
	});
    $.each(vocabulary, function(i, val) {
 		vocArr.push({"type": "ar", "word": val.arabic, "pronunciation": val.pronunciation, "lecture": val.lecture});
	});

    shuffle(vocArr);
    for (var z = 0; z < vocArr.length; z++) {
        if (vocArr[z].type == "cz"){
            $(source).append("<div id='card-"+z+"'><span class='word'>"+vocArr[z].word+"</span><br><br><span class='lecture'>lekce "+
            vocArr[z].lecture
            +"</span></div>");
        } else {
            $(source).append("<div id='card-"+z+"'><span class='word'>"+vocArr[z].word+"</span><br><br><span class='pronunciation'>[: "+
            vocArr[z].pronunciation
            +" :]</span></div>");
        }
    }
	$(source + " div").click(OpenCard);
  showPronunciation();
}


function changeCardCount(sender){
  cardCount = sender.value;
  reset();
}


function showPronunciation(){
    $('.pronunciation').each(function (){
        $(this).css({'visibility': $('#pronun').prop('checked') ? 'visible' : 'hidden'});
    });
}

$(function(){
 var m = 8;
 var cardMax = 2*Math.floor(vocabularyAll.length/m);
 for(var i = 1; i <= cardMax; i++){
    if (i*m == cardCount){
     $('#cardCount').append("<option value="+i*m+" selected>"+i*m+"</option>");
    } else {
     $('#cardCount').append("<option value="+i*m+">"+i*m+"</option>");
    }
 }
  init();
});
